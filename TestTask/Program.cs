﻿using System;
using static System.Console;

namespace TestTask
{

	/// <summary>
	/// Application root class.
	/// </summary>
	public static class Program
	{

		/// <summary>
		/// Application entry point.
		/// </summary>
		public static void Main() {
			int clientsCount = Environment.ProcessorCount * 2;
			int queriesPerClient = 1 << 10;
			Emulation.Run(clientsCount, queriesPerClient);

			ReadKey();
		}
	}
}